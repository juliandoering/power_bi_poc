# Goal 

Create a Comtras-DWH (C-DWH) to automate the reporting from Comtras programm p824

# Preconditions

- only p824
- use the existing power bi report
- use the existing star schema

# User Stories

As a C-DWH user, I want to transfer all data that are required for my report into a staging area.
	The C-DWH staging area must hold all data from P824.
	The C-DWH staging area must hold all data that are referenced by P824.

As a C-DWH user, I want to transfer the data from the staging area into the dwh area in order to create dimension tables.
	The C-DWH dwh area must create dimension tables based on the imported data in staging.
	The C-DWH dwh area must set all references in the p824 schema.
	The C-DWH dwh area must create a fact table for the p824 report.

# Questions

- where do we get the referenced data from 824?
	p824 references the accounts, but the report should also show the customer name
	where can we get the customers?
	can we do this in a automatic way?

