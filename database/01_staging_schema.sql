-- staging table for comtras program 824
-- everything is a VARCHAR because data are imported from excel
CREATE TABLE [IF NOT EXISTS] p824_01_staging (
	company VARCHAR,
	purchase_contract VARCHAR,
	sales_contract VARCHAR,
	contractor VARCHAR,
	quanity_in_bags VARCHAR
	quality_description VARCHAR,
	shipment_date VARCHAR,
	contract_value VARCHAR,
	market_value VARCHAR,
	profit_loss VARCHAR,
	profit_loss_per_bar VARCHAR,
	posting_date VARCHAR,
	quality_differential_price VARCHAR,
	differential_price_date VARCHAR,
	days_old VARCHAR,
	remarks_2 VARCHAR,
	trader VARCHAR,
	origin VARCHAR,
	quality VARCHAR,
	crop VARCHAR,
	debitor_kreditor VARCHAR,
	account VARCHAR,
	quality_type VARCHAR
);
