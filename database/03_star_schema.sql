-- dimension tables and fact table
-- dimension tables start with a d.
-- fact tables start with a f.

-- dimension: origins
CREATE TABLE d.origins (
	prov_counter INT,
	origin_code INT,
	origin_name VARCHAR,
	prov_variety INT,
	prov_group INT,
	prov_country INT,
	origin_variety VARCHAR,
	origin_region VARCHAR,
	origin_group VARCHAR,
	origin_country VARCHAR
	origin_country_code INT
);

-- dimension: agents nkg
CREATE TABLE d.agents_nkg (
	agent_counter INT,
	agent_name VARCHAR,
	agent_abbreviation VARCHAR,
	agent_own BOOLEAN NOT NULL
);

-- dimension: dates
CREATE TABLE d.dates (
	datum DATE,
	year INT,
	month_number INT,
	day_number INT,
	datum_id VARCHAR,
	weekday_number INT,
	weekday_name VARCHAR,
	month_name VARCHAR,
	week_number INT,
	month_id INT,
	week_id INT
);

-- dimension: qualities
CREATE TABLE d.qualities (
	quality_counter INT,
	origin_code INT,
	quality_code INT,
	qualtiy_crop INT,
	quality_name VARCHAR
);

-- dimension: agents br
CREATE TABLE d.agents_br (
	agent_code VARCHAR,
	agent_name VARCHAR,
	agent_acct INT
);

-- dimension: regions
CREATE TABLE d.regions (
	region_counter INT,
	reg_abbreviation VARCHAR,
	reg_description VARCHAR,
	reg_sort INT
);

-- dimension: varieties
CREATE TABLE d.varieties (
	var_counter INT,
	var_abbrev VARCHAR,
	var_descr VARCHAR,
	var_sort INT
);

-- dimension: customer
CREATE TABLE d.customer (
	customer_counter INT,
	customer_seller VARCHAR,
	customer_acct INT,
	customer_code VARCHAR,
	customer_name VARCHAR,
	customer_country_code VARCHAR (2)
	customer_d_k VARCHAR (1),
	customer_level VARCHAR,
	customer_group VARCHAR,
	customer_unit VARCHAR,
	customer_cat VARCHAR,
	customer_key_account VARCHAR,
);

-- dimension: target
CREATE TABLE d.target (
	customer_group VARCHAR,
	group_target VARCHAR,
	group_cat VARCHAR
);

-- dimension: suppliers
CREATE TABLE d.suppliers (
	supplier_counter INT,
	supplier_account INT,
	supplier_code VARCHAR,
	supplier_name VARCHAR,
	supplier_country_code VARCHAR (2),
	supplier_d_k VARCHAR (1),
	supplier_level VARCHAR,
	supplier_group VARCHAR
);

-- dimension: un country codes
CREATE TABLE d.un_country_codes (
	country_code VARCHAR,
	country_name VARCHAR
);

-- dimension: trader
CREATE TABLE d.trader (
	trader_code VARCHAR,
	trader_name VARCHAR
);

