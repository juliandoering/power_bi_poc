FROM postgres:11.9

RUN mkdir -p /docker-entrypoint-start.d/

# creat staging schema
COPY 01_staging_schema.sql /docker-entrypoint-start.d/
RUN chmod 775 /docker-entrypoint-start.d/01_staging_schema.sql

# create dwh schema
COPY 02_dwh_schema.sql /docker-entrypoint-start.d/
RUN chmod 775 /docker-entrypoint-start.d/02_dwh_schema.sql

# create star schema
copy 03_star_schema.sql /docker-entrypoint-start.d/
RUN chmod 775 /docker-entrypoint-start.d/03_star_schema.sql

CMD ["postgres"]
