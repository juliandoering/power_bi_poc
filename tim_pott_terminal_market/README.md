# Terminal Market Reporting

This directory contains the current PowerBi reporting solution from the terminal market department.
All data are collected and handled by Tim Pott.
This files are *higly confidential*!

## Files

"IAC 824 V3.pbix"
	this is the power bi file that contains the model and the report

"Dimensionstabellen.xlsx"
	data that will be filled in the snowflake schema
	one sheet for each dimension and fact
	
"P82414ACT.xlsx"
	exportet data from Comtras
	
